package com.vivo.internet.dubbo.test;

import org.apache.dubbo.config.spring.context.annotation.DubboComponentScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@DubboComponentScan({"com.vivo.internet.dubbo.test"})
public class DubboTestApplication {
    public static void main(String[] args) {
        SpringApplication.run(DubboTestApplication.class, args);
    }
}