package com.vivo.internet.dubbo.test.consumer;

import com.vivo.internet.finance.product.facade.BaseRespDTO;
import com.vivo.internet.finance.product.facade.front.api.ProductInfoFacade;
import com.vivo.internet.finance.product.facade.front.dto.resp.ProductDetailRespDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class ProductService {
    @Reference(check = false, version = "1.0.0")
    private ProductInfoFacade productInfoFacade;

    public BaseRespDTO<ProductDetailRespDTO> QueryProductInfo(String prodcutCode) {
        return productInfoFacade.queryProductDetailByCode(prodcutCode);
    }
}
