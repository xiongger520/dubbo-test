package com.vivo.internet.dubbo.test.provider;

public interface HelloService {
    public String hello(String tmp);
}
