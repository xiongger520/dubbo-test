package com.vivo.internet.dubbo.test.consumer;

import com.vivo.finance.loan.user.facade.common.UserCenterResponseWrapper;
import com.vivo.finance.loan.user.facade.dto.UserAssociationDto;
import com.vivo.finance.loan.user.facade.dto.UserInfoDto;
import com.vivo.finance.loan.user.facade.dubbo.UserInfoApi;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class UserService {

    @Reference(check = false, version = "1.0.0")
    private UserInfoApi userInfoApi;

    //编辑用户信息
    public UserCenterResponseWrapper<Boolean> EditUserInfo(UserInfoDto userInfoDto) {
        return userInfoApi.editUserInfo(userInfoDto);
    }

    //编辑用户关系
    public UserCenterResponseWrapper<Boolean> EditUserAssociation() {
        UserAssociationDto userAssociationDto = new UserAssociationDto();
        return userInfoApi.editUserAssociation(userAssociationDto);
    }

    public void hello() {
        System.out.println("hello world!");
    }
}
