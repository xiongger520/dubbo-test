package com.vivo.internet.dubbo.test.provider.impl;

import com.vivo.internet.dubbo.test.provider.HelloService;
import org.apache.dubbo.config.annotation.Service;

@Service(version = "1.0.0")
public class HelloServiceImpl implements HelloService {

    @Override
    public String hello(String tmp) {
        return "hello world " + tmp;
    }

}
