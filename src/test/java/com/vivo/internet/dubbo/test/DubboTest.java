package com.vivo.internet.dubbo.test;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.vivo.finance.loan.user.facade.constant.*;
import com.vivo.finance.loan.user.facade.dto.UserInfoDto;
import com.vivo.internet.dubbo.test.consumer.ProductService;
import com.vivo.internet.dubbo.test.consumer.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DubboTest {

    @Autowired
    private UserService userService;

    @Test
    public void UserServiceTest() {
        UserInfoDto dto = new UserInfoDto();
        dto.setOpenId("&&&");//必传
        //dto.setMemberNo("333");
        dto.setChannelType(ChannelTypeConstant.vivoPay);//必传
        dto.setVendorCode(VendorConstant.bisness);//必传
        dto.setCertificateType(CertificateTypeConstant.ID);//默认身份证，不可为空
        dto.setCertificateNumber("230011199909070065");//身份证号，必传
        //dto.setNation(NationConstant.Han);//昵称
        //dto.setUserGrade(1L);//用户等级
        //dto.setUserScore(35L);
        dto.setUserRole(UserRoleConstant.GENERAL_USER);//用户角色，例如：普通用户、管理员，默认普通用户
        //dto.setAccoutStatus(1);//账户状态，1为启用，0为禁用列入黑名单，默认1
        //dto.setCertificationStatus(0);//实名状态，1为已实名，0为未实名，默认0
        //dto.setBankCard("124123135412");//默认银行卡卡号
        //dto.setBankCardId("124123135412");//用户默认银行卡id
        dto.setName("哪吒");//用户名
        //dto.setSex(SexTypeConstant.UNKNOWN);
        //dto.setHouseholdType(HouseholdTypeConstant.NON_AGRICULTURAL_HOUSEHOLD);//户籍类型，例如：非农业户口、农业户口，个人用户专用字段
        //dto.setNationalityZh(StateEnum.China.getZh());//国籍（中文），默认中华人民共和国
        //dto.setNationalityEn(StateEnum.China.name());//国籍（英文），默认CHINA
        //dto.setStateCode(StateEnum.China.getEn());//国家或地区代码，默认CHN
        dto.setIsAdult(ChoiceTypeConstant.YES);//是否成年人，1为是，0为否，默认1，个人用户专用字段
        //dto.setPoliticalStatus(PoliticalStatusConstant.QZ);
        //dto.setEducation(EducationConstant.UNDERGRADUATE);
        System.out.println(JSON.toJSONString(userService.EditUserInfo(dto),
                SerializerFeature.PrettyFormat,
                SerializerFeature.WriteMapNullValue));
    }

    @Autowired
    private ProductService productService;

    @Test
    public void ProductServiceTest() {
        System.out.println(JSON.toJSONString(productService.QueryProductInfo("ZJAQX001"),
                SerializerFeature.PrettyFormat,
                SerializerFeature.WriteMapNullValue));
    }
}

